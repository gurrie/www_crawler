# Scrapy settings for www_crawler project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'www_crawler'

SPIDER_MODULES = ['www_crawler.spiders']
NEWSPIDER_MODULE = 'www_crawler.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'www_crawler (+http://www.yourdomain.com)'
