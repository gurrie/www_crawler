from www_crawler.items import WwwCrawlerItem
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor 

from scrapy.link import Link 

from urllib import quote
from urlparse import urlparse, urlunparse

class WwwSpider(CrawlSpider):
    name = "o2"
    allowed_domains = ["www.o2.co.uk"]
    handle_httpstatus_list = [404, 500]
    start_urls = ["https://www.o2.co.uk/shop"]

    rules = (
        Rule(SgmlLinkExtractor(allow=('/shop')), callback='parse_response', process_links='encode_links', follow=True),
    ) 
    
    def parse_response(self, response):
	if response.status in [404, 500]: 
            yield self.extract_item(response)

    def encode_links(self, links):
	return map(self.encode_link, links) 	
    
    def encode_link(self, link): 
        scheme, netloc, path, params, query, fragment = urlparse(link.url)	
	
	if 'pcImageUrl' in path:
	    path = quote(path)

	encoded_url = urlunparse((scheme, netloc,  path,  params,  query,  fragment))
       
	return Link(encoded_url, link.fragment, link.nofollow)
 
    def extract_item(self, response):
	referer = response.request.headers.get('Referer', None)

	if referer != None and 'shop' not in referer:
	    return None

        item = WwwCrawlerItem()
	item['status'] = response.status
	item['url'] = response.url
        item['referer'] = referer 
 
	return item
